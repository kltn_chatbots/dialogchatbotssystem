"""DialogChatBotsSystem URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from DialogChatBotsSystem import settings
from WebMessageServices import routers

urlpatterns = [
    path('admin/', admin.site.urls),
    path('dashboard/', include("WebDashboard.urls")),
    # Django Rest Framework
    path('api/', include(routers.router.urls)),
    path('api/', include("WebMessageServices.urls")),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('froala_editor/', include('froala_editor.urls')),
]
handler404 = 'WebDashboard.views.error'
handler500 = 'WebDashboard.views.error'

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
