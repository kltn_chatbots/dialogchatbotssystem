from django.contrib import admin

from WebDashboard.models import *

# Register your models here.

admin.site.register(
    [StoreFile,
     ConversationLog, NotificationDevice,
     Person, SenderGroup,
     NotificationContent, Menu
     ])


class ChatMessageViewAdmin(admin.ModelAdmin):
    list_display = ['questionMessage', 'answerProcessed']
    list_filter = ['questionMessage', 'timeDate']
    search_fields = ['questionMessage', 'answerMessage']


admin.site.register(ChatMessage, ChatMessageViewAdmin)
