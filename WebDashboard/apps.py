from django.apps import AppConfig


class WebdashboardConfig(AppConfig):
    name = 'WebDashboard'
