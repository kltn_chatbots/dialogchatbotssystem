from django import forms
from django.forms import widgets

from WebDashboard.models import ChatMessage
from libs.pyvi.ViTokenizer import ViTokenizer
from utils import stringPlaintextUtil


class InputDataTrainForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(InputDataTrainForm, self).__init__(*args, **kwargs)
        attrs = {
            'type': 'text',
            'class': 'form-control',
            'aria-describedby': 'emailHelp',
            'placeholder': 'Input data question',
        }

        textInput = widgets.TextInput(attrs)
        self.fields['questionMessage'].widget = textInput

    def save(self, commit=True):
        dataForm = super().save(commit=False)
        dataForm.questionProcessed = ViTokenizer.tokenize(stringPlaintextUtil.removeMark(dataForm.questionMessage))
        answerPlaintext = stringPlaintextUtil.removeTags(dataForm.answerMessage)
        dataForm.answerProcessed = ViTokenizer.tokenize(stringPlaintextUtil.removeMark(answerPlaintext))
        dataForm.save()  # save to database

    class Meta:
        model = ChatMessage
        fields = ['questionMessage', 'answerMessage']
