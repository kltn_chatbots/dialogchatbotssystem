import froala_editor
from django.core.validators import RegexValidator
from django.db import models
from djongo import models as djongo

# Create your models here.
from froala_editor.fields import FroalaField


class StoreFile(models.Model):
    nameFileUpload = models.CharField(max_length=1000)
    urlPath = models.CharField(max_length=1000)  # url get file view browser
    sizeFile = models.FloatField()
    extension = models.CharField(max_length=8)
    dateTime = models.DateTimeField(auto_now_add=True)


class FeedBack(models.Model):
    rateMark = models.IntegerField(default=0)
    description = models.TextField()
    dateTime = models.DateTimeField(auto_now=True)

    class Meta:  # NTT embedded ConversationLog
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Messages(models.Model):
    question = models.TextField()
    answer = models.TextField()

    class Meta:
        abstract = True

    def __str__(self):
        return 'question :: {question} -> answer :: {answer}'.format(question=self.question, answer=self.answer)


class ConversationLog(models.Model):
    # person = models.ManyToOneRel('person', to='Person', field_name='_id')
    feedback = djongo.EmbeddedModelField(
        model_container=FeedBack,
    )
    dateTime = models.DateTimeField(auto_now_add=True)
    history_message = djongo.ArrayModelField(
        model_container=Messages
    )

    def __str__(self):
        return str(self.history_message)

    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question', None)
        answer = kwargs.pop('answer', None)
        super().__init__(*args, **kwargs)
        if self.feedback is None:
            self.feedback = FeedBack(None)
        if not question is None and not answer is None:
            self.history_message = [Messages(question=question, answer=answer)]
        elif self.history_message is None:
            self.history_message = []

    def add_history_mesage(self, question, answer):
        if not question is None and not answer is None:
            self.history_message.append(Messages(question=question, answer=answer))
            # self.save()


class NotificationDevice(models.Model):
    idClientDevice = models.CharField(max_length=200)
    optionNotify = models.BooleanField(default=True)

    def __str__(self):
        return str.format("Device ID :: ", self.idClientDevice)


class Person(models.Model):
    name = models.CharField(max_length=2000)
    province = models.TextField()
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    numberPhone = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    email = models.EmailField()
    devices = djongo.ForeignObject(
        to=NotificationDevice,
        from_fields=["person_devices"],
        to_fields=["id"],
        on_delete=djongo.CASCADE)

    def __str__(self):
        return self.name


class SenderGroup(models.Model):  # as group sender
    name = models.CharField(max_length=200)
    listIdDevices = djongo.ArrayModelField(
        model_container=NotificationDevice
    )
    # TODO NTT continue handle....


class NotificationContent(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    dateTime = models.DateTimeField(auto_now_add=True)
    senderGroup = models.ManyToManyField(SenderGroup)

    def __str__(self):
        return self.title


class Menu(models.Model):
    title = models.CharField(max_length=200)
    keyName = models.CharField(max_length=200)
    description = models.TextField()

    # TODO NTT handle continue....

    def __str__(self):
        return self.keyName


class ChatMessage(models.Model):
    questionMessage = models.TextField()
    answerMessage = FroalaField(plugins=froala_editor.PLUGINS,
                                options={
                                    'toolbarInline': False,
                                })
    questionProcessed = models.TextField()
    answerProcessed = models.TextField()
    timeDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.questionMessage


class DictionaryWord(models.Model):
    word = models.TextField()
    value = models.IntegerField(db_index=True)
    mapping = djongo.ListField(models.TextField())
    updateTime = models.DateTimeField(auto_now=True)

    def mapping_insert(self, word):
        self.mapping.insert(len(self.mapping), word)
        self.save()

    def mapping_remove(self, word):
        self.mapping.remove(word)
        self.save()

    def __str__(self):
        return '\nword ::: {0} value: {1} mapping: [{2}]'.format(self.word, self.value, self.mapping)
