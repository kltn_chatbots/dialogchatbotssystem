from rest_framework import serializers

from WebDashboard.models import DictionaryWord


class DictionarySerializer(serializers.Serializer):
    word = serializers.CharField(required=True, allow_blank=False, max_length=2000)
    value = serializers.IntegerField(required=True)
    mapping = serializers.ListField(serializers.CharField(required=False, allow_blank=True, max_length=2000))

    @staticmethod
    def getDataFromWord(word_search=''):
        word_search = word_search.replace(' ', '_')  # TODO NTT model pyvi
        listWord = DictionaryWord.objects.filter(word__icontains=word_search);
        result = DictionarySerializer(
            [dict(word=item.word, value=item.value, mapping=item.mapping) for item in listWord.iterator()],
            many=True
        )
        print('getDataFromWord list word from database :::', listWord, ' data to JSON: ', result)
        return result
