import os
import pickle

from DialogChatBotsSystem import settings
from WebDashboard.models import DictionaryWord

DEFAULT_DATA_FILE_PATH = os.path.join(settings.BASE_DIR, 'libs\seq2seq_core\data')


def load_data_dictionary_words(folderPath=DEFAULT_DATA_FILE_PATH):
    print('WebDashboard.services.load_data_dictionary_words from path :::', folderPath,
          ' path project : ', settings.BASE_DIR)
    # read data control dictionaries
    try:
        with open(os.path.join(folderPath, 'metadata.pkl'), 'rb') as f:
            metadata = pickle.load(f)
    except:
        metadata = None

    return metadata


def import_file_data_to_database(folderPath=DEFAULT_DATA_FILE_PATH):
    metadata = load_data_dictionary_words()

    w2idx = metadata['w2idx']  # w2idx [{'key':value}]

    listWord = []
    for (key, value) in w2idx.items():
        word = DictionaryWord(word=key, value=value, mapping=[])
        listWord.append(word)

    # Call bulk_create to create records in a single call
    DictionaryWord.objects.bulk_create(listWord)


# ======================================================================================================================
# DictionaryWord class method implement
# ======================================================================================================================

def remove_dictionary_record(word, value):
    try:
        wordDB = DictionaryWord.objects.get(word__exact=word, value__exact=value)
    except DictionaryWord.DoesNotExist:
        return False
    if wordDB is not None and len(wordDB.mapping) == 0:
        wordDB.delete()
        return True
