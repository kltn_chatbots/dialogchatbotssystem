from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('', views.empty, name='empty'),
    path('analytic', views.analytic, name='analytic'),
    path('input', views.inputDataTrain, name='input'),
    path('dictionary', views.dictionary, name='dictionary'),

    path('dictionary_api', views.dictionary_api, name='dictionary_api'),
    path('dictionary_api/load_file', views.dictionary_database_load_file, name='dictionary_load_file'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
