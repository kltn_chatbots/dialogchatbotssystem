import logging

from django.http import HttpResponseRedirect
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

from WebDashboard.forms import InputDataTrainForm
from WebDashboard.serializers import DictionarySerializer
from WebDashboard.services import *
# Get an instance of a logger
from utils import stringPlaintextUtil

logger = logging.getLogger(__name__)

def analytic(request):
    return render(request, 'content_app_main/analytics-content.html')


def inputDataTrain(request):
    form = InputDataTrainForm()

    # If this is a POST request then process the Form data
    if request.method == "POST":
        # Create a form instance and populate it with data from the request (binding):
        form = InputDataTrainForm(request.POST)
        print("NTT submit request.method == POST :: post ", request.POST)
        # Check if the form is valid:
        if form.is_valid():
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect(request.path)

    return render(request, 'content_editor/input-data-train-content.html', {"inputDataTrainForm": form})


def dictionary(request):
    return render(request, 'content_editor/dictionary-content.html', {'title': 'Dictionary words'})
def empty(request):
    return render(request, 'empty-content-sample.html')


# ======================================================================================================================
#                                           API RESPONSE JSON DATA                                                     #
# ======================================================================================================================
MAPPED_CODE = 0  # Mapped
MAPPING_CODE = 1  # To Mapping
ADD_NEW_CODE = 2  # Add New
ADD_NEW_VALUE = -1  # is word_value default -1 as NULL


@api_view(['POST', 'PUT'])
def dictionary_api(request, format=None):
    # Retrieve, update or delete a code snippet.
    if request.method == 'POST':
        print('NTT dictionary_api data request POST ::: ', request.data)
        wordSearch = request.data['word'];
        return Response(DictionarySerializer.getDataFromWord(wordSearch).data)

    elif request.method == 'PUT':
        print('NTT dictionary_api data request PUT ::: ', request.data)
        # request.data {'primary_word': {'word': 'Truong SPKT', 'value': 4},
        #               'mapping_word': [{'word': 'SPKT', 'value': '5', 'status': 1}]}
        word = request.data['primary_word']['word']
        value = request.data['primary_word']['value']
        wordPrimary = DictionaryWord.objects.get(word__exact=word, value__exact=value)
        mappingWords = request.data['mapping_word']
        print('NTT dictionary_api data request PUT mappingWords::: ', mappingWords, 'wordPrimary:', wordPrimary)

    for item in mappingWords:
        word = item['word']
        value = item['value']
        status = item['status']
        if status == MAPPING_CODE:
            success = remove_dictionary_record(word, value)
            if success:
                wordPrimary.mapping_insert(word)
            else:
                logger.error('dictionary_api remove_dictionary_record failed item data :::', item)
                err = 'Add key mapping is key primary. Or word mapping not exists.'
                return Response(dict({'status': False, 'messages': err}), status=status.HTTP_204_NO_CONTENT)
        else:
            wordPrimary.mapping_insert(stringPlaintextUtil.optimizeText(word).replace(' ', '_'))
    return Response(dict({'status': True}))


@api_view(['POST'])
def dictionary_database_load_file(request, format=None):
    # Retrieve, update or delete a code snippet.
    if request.method == 'POST':
        print('NTT dictionary_load_file data request POST ::: ', request.data)
        import_file_data_to_database()
        return Response(dict({'states': True}))
