from django.apps import AppConfig


class WebmessageservicesConfig(AppConfig):
    name = 'WebMessageServices'
