from rest_framework import serializers


class AnswerRequestSerializer(serializers.Serializer):
    id_msg_log = serializers.IntegerField(allow_null=True, required=False, default=-1)
    question = serializers.CharField(required=False, allow_blank=True, max_length=2000)
    answer = serializers.CharField(required=False, allow_blank=True, max_length=2000)
    request_time = serializers.DateTimeField(required=False)

    class Meta:
        fields = ('id_msg_log', 'question', 'answer')
