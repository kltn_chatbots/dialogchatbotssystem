from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('request', views.RequestAnswers.as_view(), name='request'),
    path('message_chatbots', views.message_chatbots, name='message_chatbots'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
