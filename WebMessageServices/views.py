from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from WebDashboard.models import ConversationLog
from WebMessageServices.serializers import AnswerRequestSerializer
from libs.seq2seq_core import predict_seq2seq_core


class RequestAnswers(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """

    def post(self, request, format=None):
        body = AnswerRequestSerializer(data=request.data)
        # body =
        # {
        #     "id_msg_log": "MD5 general with IP and time start",
        #     "question": "Sư phạm kỹ thuật thành phố hồ chí minh ở đâu",
        #     "answer": "Sư phạm kỹ thuật",
        #     "request_time": "1987-09-03T00:00:00.000Z"
        # }

        print('RequestAnswers post data ::: {body}'.format(body=body))
        if body.is_valid():
            data = body.data
            question = data['question']
            answer, _ = predict_seq2seq_core.predict_chatbots(question)
            # answer = ' câu trả lời 22222222222222222222222222'
        else:
            return Response(body.errors, status=status.HTTP_400_BAD_REQUEST)

        if 'id_msg_log' in data.keys() and data.get('id_msg_log', -1) > -1:
            id_msg_log = data['id_msg_log']
            try:
                con = ConversationLog.objects.get(pk=id_msg_log)
            except ConversationLog.DoesNotExist as err:
                print('RequestAnswers post error except ConversationLog.DoesNotExist :::', err)
                return Response(err.errors, status=status.HTTP_204_NO_CONTENT)
            con.add_history_mesage(question, answer)
            con.save()
            res = AnswerRequestSerializer(data={'answer': answer, 'id_msg_log': con.pk})
            if not res.is_valid():
                return Response(res.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(res.data, status=status.HTTP_201_CREATED)
        else:
            con = ConversationLog(question=question, answer=answer)
            con.save()
            res = AnswerRequestSerializer(data={'answer': answer, 'id_msg_log': con.pk})
            if not res.is_valid():
                return Response(res.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(res.data, status=status.HTTP_201_CREATED)
        return Response(request.data, status=status.HTTP_202_ACCEPTED)


def message_chatbots(request):
    return render(request, 'message_chatbots/index.html')