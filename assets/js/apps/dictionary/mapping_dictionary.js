var SERVER_URL = 'http://127.0.0.1:8000'

function showSnackbarError(errorString) {
    new PNotify({
        text: errorString,
        confirm: {
            confirm: true,
            buttons: [
                {
                    text: 'Dismiss',
                    addClass: 'btn btn-link',
                    click: function (notice) {
                        notice.remove();
                    }
                },
                null
            ]
        },
        buttons: {
            closer: false,
            sticker: false
        },
        animate: {
            animate: true,
            in_class: 'slideInDown',
            out_class: 'slideOutUp'
        },
        addclass: 'md'
    });
}

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function optimizeKeyId(string_key) {
    return string_key.replace(' ', '_')
}

function callAjaxSearch(url, dataPost, csrf_token, callback) {
    $.ajax({
        url: url,
        headers: {"X-CSRFToken": csrf_token},
        data: dataPost,
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: callback,
        error: function (err) {
            console.log('map-key-dictionary searchWordDictionaryPrimary error ::: ', err);
            showSnackbarError('Connection timed out. Please contact admin.')
        }
    });
}

function searchWordDictionaryPrimary(url, csrf_token, event) {
    if (event.keyCode !== 13) {
        return
    }
    wordData = document.getElementsByName('searchWordDictionaryPrimary')[0].value;
    dataPost = JSON.stringify({word: wordData});
    console.log("map-key-dictionary searchWordDictionaryPrimary call POST data ::: ", dataPost);

    callAjaxSearch(url, dataPost, csrf_token, function (datas) {
        console.log('Data response ::: ', datas)

        $('#listSearchPrimary').children().remove()
        datas.valueOf().forEach(function (item) {
            var MAPPING_DOM = (item.mapping.length>0) ? ('mapping_data="' + item.mapping + '"'):'';
            $('#listSearchPrimary').append(
                '<li name="itemSearch" id="itemSearch" class="ripple flex-wrap flex-sm-nowrap row p-2 no-gutters align-items-center fuse-ripple-ready" word_value="'
                + item.value + '" onclick="setValueItemNew($(this))" ' + MAPPING_DOM + '>' + item.word + '</li>')
        });

        // NTT DOM script re-attack event animation of template
        $('#listSearchPrimary').append('<script type="text/javascript" src="../assets/vendor/fuse-html/fuse-html.min.js"/>')
    })
}

function handleEnterKey(item, event) {
    if (event.keyCode === 13) {
        addNewWordToDictionary(item[0].id)
    }
}

function addNewWordToDictionary(idElement) {
    itemInput = $('#' + idElement)
    console.log('addNewWordToDictionary value ::: ', idElement, itemInput, itemInput.val())
    data = {word_key: itemInput.val(), word_value: 'NULL'}
    addDataToTable(data, 2);
}


function createTable(idTable, idBodyTable) {
    headerTable =
        '<thead>\n' +
        '<tr>\n' +
        '    <th class="secondary-text">\n' +
        '        <div class="table-header">\n' +
        '            <span class="column-title">Word key</span>\n' +
        '        </div>\n' +
        '    </th>\n' +
        '    <th class="secondary-text">\n' +
        '        <div class="table-header">\n' +
        '            <span class="column-title">Word value</span>\n' +
        '        </div>\n' +
        '    </th>\n' +
        '    <th class="secondary-text">\n' +
        '        <div class="table-header">\n' +
        '            <span class="column-title">Status</span>\n' +
        '        </div>\n' +
        '    </th>\n' +
        '    <th class="secondary-text">\n' +
        '        <div class="table-header">\n' +
        '            <span class="column-title">Actions</span>\n' +
        '        </div>\n' +
        '    </th>\n' +
        '</tr>\n' +
        '</thead>';
    table =
        '<table id="' + idTable + '" class="table">' +
        headerTable +
        '<tbody id="' + idBodyTable + '">' +
        '</tbody>' +
        '</table>'
    return table;
}

function removeItemPreView(selfCloseButton) {
    var itemPreView = selfCloseButton.closest('div.card');
    itemPreView.remove();
}

function addViewPreSave(word_key, word_value) {
    BTN_SEND =
        '<button type="button" class="btn btn-secondary btn-fab btn-sm" onclick=submitUpdataWordValue($(this))>' +
        '        <i class="icon-send"></i>' +
        '</button>'
    BTN_CLOSE = '<i class="icon-close close mr-2" onclick="removeItemPreView($(this))"></i>'
    idSeachBox = 'idMapNewWordForInput' + optimizeKeyId(word_key)
    SEARCH_BOX =
        '<div class="md-elevation-1 row no-gutters align-items-center w-50 p-2 mr-10">\n' +
        '    <input class="col pl-2" type="text" placeholder="Add new word to dictionary..." required\n' +
        '           id="' + idSeachBox + '" onkeyup="handleEnterKey($(this), event)" onclick="function f() { return }" style="background: transparent">\n' +
        '    <i class="icon-tooltip-outline-plus" onclick="addNewWordToDictionary(\'' + idSeachBox + '\')"></i>\n' +
        '</div>';
    DIV1 = '<div class="card" word_key="' + word_key + '" word_value="' + word_value + '">';
    DIV11 =
        '<div class="card-header" role="tab" id="heading' + optimizeKeyId(word_key) + '" aria-expanded="true" aria-controls="collapse' + optimizeKeyId(word_key) + '" onclick="setValueItemSelect(\'' + word_key + '\',\'' + word_value + '\')">' +
        '<a data-toggle="collapse" href="#collapse' + optimizeKeyId(word_key) + '" class="collapsed" style="display: flex; justify-content: space-between; text-decoration: none;">' +
        '<div class="row w-50">' + BTN_CLOSE +
        '<strong word_key="collapse' + optimizeKeyId(word_key) + '" word_value="' + word_value + '" style="display: flex;  align-items: center;">' + word_key + '</strong> </div>' + SEARCH_BOX + BTN_SEND + '</a> </div>';

    DIV12 =
        '<div id="collapse' + optimizeKeyId(word_key) + '" class="collapse show" role="tabpanel" aria-labelledby="heading' + optimizeKeyId(word_key) + '" data-parent="#listPreViewMapping"> ' +
        '   <div class="card-block">' + createTable('idTable' + optimizeKeyId(word_key), 'idBodyTable' + optimizeKeyId(word_key)) +
        '   </div>' +
        ' </div> </div>';

    $('#listPreViewMapping').append(DIV1 + DIV11 + DIV12)
    $('#idTable' + optimizeKeyId(word_key)).DataTable({
        responsive: true,
        dom: 'rt<"dataTables_footer"ip>',
        column: ['word_key', 'word_value', 'status', 'actions']
    });
}

function setValueItemNew(self) {
    var word_key = self.text()
    var word_value = self.attr('word_value')
    console.log("setValueItemSelect(self) item click::", word_key)

    setValueItemSelect(word_key, word_value)

    addViewPreSave(word_key, word_value)

    var mapping_data = self.attr('mapping_data')
    if (typeof mapping_data !== typeof undefined && mapping_data !== false){
        mapping_data = mapping_data.split(',')
        console.log('setValueItemNew mapping_data', mapping_data)
        mapping_data.forEach(function (item) {
            addDataToTable({
                    word_key: item,
                    word_value: 'NULL'
                },
                MAPPED_CODE
            )
        })
    }
}

// render view current word sellect from searchWordDictionaryPrimary
function setValueItemSelect(word_key, word_value) {
    console.log("map-key-dictionary setValueItemSelect(word_key, word_value) item click::", word_key)
    var elementChose = $('#textChoose');
    elementChose.text(word_key)
    elementChose.attr('word_value', word_value)
}

function addDataToTableMapping(itemElementHtml) {
    data = {
        word_key: itemElementHtml.text(),
        word_value: itemElementHtml.attr('word_value')
    }
    addDataToTable(data, MAPPING_CODE)
    /*status 1: Mapping*/
}

function removeSelfRow(self) {
    console.log('removeSelfRow of table id :::', self, self.parents('table').attr('id'), self.parents('tr'), self.closest('tr')[0])
    $('#' + self.parents('table').attr('id')).DataTable().row(self.closest('tr')).remove().draw();
}


MAPPED = 'Mapped';
MAPPED_CODE = 0; // status
MAPPING = 'To Mapping';
MAPPING_CODE = 1;
ADD_NEW = 'Add New';
ADD_NEW_CODE = 2;
ADD_NEW_VALUE = -1; // is word_value default -1 as NULL

/*data = {
    word_key: 'word key',
    word_value: 1
}
status: 1 /!*status 0: Mapped, 1: Mapping, 2: Add New*!/ */
function addDataToTable(data, status) {
    var word_key = $('strong#textChoose').text()
    var idTable = '#idTable' + optimizeKeyId(word_key);
    var idBodyTable = '#idBodyTable' + optimizeKeyId(word_key);
    console.log('addDataToTable idTable ::: ', optimizeKeyId(word_key), ' data: ', data)

    iconMapped = '<i class="quantity-indicator icon-circle s-3 text-success mr-1"></i>' + MAPPED
    iconMapping = '<i class="quantity-indicator icon-circle s-3 text-warning mr-1"></i>' + MAPPING
    iconAddNew = '<i class="quantity-indicator icon-circle s-3 text-info mr-1"></i>' + ADD_NEW
    buttonDetack = '<button class="badge badge-pill badge-danger p-2 pl-4 pr-4">Detack</button>\n'
    buttonCancel = '<button class="badge badge-pill badge-info p-2 pl-4 pr-4" onclick="removeSelfRow($(this))">Cancel</button>\n'
    buttonRemove = '<button class="badge badge-pill badge-warning p-2 pl-4 pr-4" onclick="removeSelfRow($(this))">Remove</button>\n'

    // htmlElementDom =
    //     '<tr>\n' +
    //     '    <td>' + data.word_key + '</td>\n' +
    //     '    <td>' + data.word_value + '</td>\n' +
    //     '    <td>\n' +
    //     ((data.status === 0) ? iconMapped : (data.status === 1) ? iconMapping : iconAddNew) +
    //     '    </td>\n' +
    //     '    <td>\n' +
    //     ((data.status === 0) ? buttonDetack : (data.status === 1) ? buttonCancel : buttonRemove) +
    //     '    </td>\n' +
    //     '</tr>'

    $(idTable).DataTable().row.add([
        data.word_key,
        data.word_value,
        ((status === MAPPED_CODE) ? iconMapped : (status === MAPPING_CODE) ? iconMapping : iconAddNew),
        ((status === MAPPED_CODE) ? buttonDetack : (status === MAPPING_CODE) ? buttonCancel : buttonRemove)
    ]).draw();
}

function searchWordDictionaryMapping(url, csrf_token, event) {
    if (event.keyCode !== 13) {
        return
    }
    wordData = document.getElementsByName('searchWordDictionaryMapping')[0].value;
    var word_key_primary = $('strong#textChoose').text();
    var word_value_primary = $('strong#textChoose').attr('word_value');
    var dataPost = JSON.stringify({
        word: wordData,
        word_exception: word_key_primary,
        value_exception: word_value_primary
    });
    console.log("map-key-dictionary searchWordDictionaryPrimary call POST data ::: ", dataPost, url);

    callAjaxSearch(url, dataPost, csrf_token, function (datas) {
        console.log('Data response ::: ', datas);
        var listSearchMapping = $('#listSearchMapping')
        $('#listSearchMapping').children().remove()
        datas.valueOf().forEach(function (item) {
            dataAddTablePreView = {word_key: item.word, word_value: item.value, status: 1};
            $('#listSearchMapping').append(
                '<li name="itemSearch" id="itemSearch" class="ripple flex-wrap flex-sm-nowrap row p-2 no-gutters align-items-center fuse-ripple-ready" word_value="'
                + item.value + '" onclick="addDataToTableMapping($(this))">' + item.word + '</li>')
            /*addDataToTable($(this),1) status=1 Mapping*/
        });

        // NTT DOM script re-attack event animation of template
        listSearchMapping.append('<script type="text/javascript" src="../assets/vendor/fuse-html/fuse-html.min.js"/>')
    })
}

// status: 1 /!*status 0: Mapped, 1: Mapping, 2: Add New*!/ */
function typeOfStatus(statusHtml) {
    var status = MAPPED_CODE;
    if (statusHtml.includes(MAPPING)) {
        status = MAPPING_CODE;
    } else if (statusHtml.includes(ADD_NEW)) {
        status = ADD_NEW_CODE;
    }
    return status
}

function callAjaxUpdate(datas, callback) {
    var url = SERVER_URL + '/dashboard/dictionary_api'
    console.log('callAjaxUpdate datas :::', datas)
    $.ajax({
        url: url,
        headers: {"X-CSRFToken": csrftoken},
        data: JSON.stringify(datas),
        type: 'PUT',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: callback,
        error: function (err) {
            console.log('callAjaxUpdate error ::: ', err);
            showSnackbarError('Connection timed out. Please contact admin.')
        }
    });
}

function submitUpdataWordValue(elementHeaderButton) {
    console.log('submitUpdataWordValue() click item ::: ', elementHeaderButton)
    var itemPrimary = elementHeaderButton.closest('div.card');
    var dataViews = itemPrimary.children('div.collapse').children('div.card-block').children('div').children('table.table').DataTable().rows().data()
    console.log("submitUpdataWordValue data ::: ", dataViews)
    var datas = [];
    for (var i = 0; i < dataViews.length; i++) {
        var status = typeOfStatus(dataViews[i][2]);
        if (status != MAPPED_CODE)
            var data = {
                word: dataViews[i][0],
                value: ((status == MAPPING_CODE) ? dataViews[i][1] : ADD_NEW_VALUE),
                status: status
            }
        datas.push(data)
    }
    if (datas.length > 0) {
        console.log('submitUpdataWordValue() data call api :::', datas)
        callAjaxUpdate({
            primary_word: {word: itemPrimary.attr('word_key'), value: Number(itemPrimary.attr('word_value'))},
            mapping_word: datas
        }, function (data) {
            console.log('submitUpdataWordValue() ajax called success', data);
            // NTT TODO HardCode auto success is current not check success
            if (data.status) {
                itemPrimary.remove()
                return;
            }
            showSnackbarError(data.messages)
        })
    }

}