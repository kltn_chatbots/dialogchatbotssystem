# model seq2seq config
emb_dim = 1024
batch_size = 32

# training config
n_epoch = 80

# data processing tokens express
PAD = '_'  # not in dictionary, unknown
UNK = 'unk'  # not in dictionary, unknown
SPA=' '
VOCAB_SIZE = 6000

NOT_RESOLVE_ANSWERS = \
    u'Hiện tại chưa có dữ liệu về câu hỏi này, mời bạn trao đổi trực tiếp với chúng tôi tại tuyensinh.hcmute.edu.vn.'
