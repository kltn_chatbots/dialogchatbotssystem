import logging
import os

import tensorflow as tf
import tensorlayer as tl

from DialogChatBotsSystem import settings
from libs.pyvi import ViTokenizer
from libs.seq2seq_core import model_config
from libs.seq2seq_core import model_seq2seq
from libs.seq2seq_core.data import data_processing
from utils import stringPlaintextUtil

logger = logging.getLogger(__name__)

# idx2w_cache_key = 'idx2w_cache'
# idx2w = cache.get(idx2w_cache_key)
# w2idx_cache_key = 'w2idx_cache'
# w2idx = cache.get(w2idx_cache_key)
#
# xvocab_size_cache_key = 'xvocab_size_cache'
# xvocab_size = cache.get(xvocab_size_cache_key)
#
# max_le_question_cache_key = 'max_le_question_cache'
# max_len_question = cache.get(max_le_question_cache_key)
# max_len_answers_cache_key = 'max_len_answers_cache'
# max_len_answers = cache.get(max_len_answers_cache_key)
#
# start_id_cache_key = 'start_id_cache'
# start_id = cache.get(start_id_cache_key)
# end_id_cache_key = 'end_id_cache'
# end_id = cache.get(end_id_cache_key)

is_reload_data = True
if is_reload_data:
    metadata, _, _ = data_processing.load_data(PATH=os.path.join(settings.BASE_DIR, 'libs/seq2seq_core/data/'))
    w2idx = metadata['w2idx']  # dict  word 2 index
    idx2w = metadata['idx2w']  # list index 2 word

    xvocab_size = len(idx2w)  # 8002 (0~8001)
    unk_id = w2idx[model_config.UNK]  # 1
    pad_id = w2idx[model_config.PAD]  # 0
    start_id = xvocab_size  # 8002
    end_id = xvocab_size + 1  # 8003
    w2idx.update({'start_id': start_id})
    w2idx.update({'end_id': end_id})
    idx2w = idx2w + ['start_id', 'end_id']
    xvocab_size = xvocab_size + 2
    max_len_question = metadata['limit']['maxq']
    max_len_answers = metadata['limit']['maxa']

    # cache.set(idx2w_cache_key, idx2w, None)  # save in the cache
    # cache.set(w2idx_cache_key, w2idx, None)
    # cache.set(xvocab_size_cache_key, xvocab_size, None)
    # cache.set(max_le_question_cache_key, max_len_question, None)
    # cache.set(max_len_answers_cache_key, max_len_answers, None)
    # cache.set(start_id_cache_key, start_id, None)
    # cache.set(end_id_cache_key, end_id, None)


#  NTT / End data processing

def config_model():
    print('libs.seq2seq_core.predict_seq2seq_core.config_model reload config_model')
    # Blueprint model
    # model for training
    encode_seqs = tf.placeholder(dtype=tf.int64, shape=[model_config.batch_size, None], name="encode_seqs")
    decode_seqs = tf.placeholder(dtype=tf.int64, shape=[model_config.batch_size, None], name="decode_seqs")
    # target_seqs = tf.placeholder(dtype=tf.int64, shape=[model_config.batch_size, None], name="target_seqs")
    # target_mask = tf.placeholder(dtype=tf.int64, shape=[model_config.batch_size, None],
    #                              name="target_mask")  # tl.prepro.sequences_get_mask()
    net_out, _ = model_seq2seq.model(encode_seqs, decode_seqs, xvocab_size, is_train=True, reuse=False)
    # model for inferencing -> predict
    encode_seqs2 = tf.placeholder(dtype=tf.int64, shape=[1, None], name="encode_seqs")
    decode_seqs2 = tf.placeholder(dtype=tf.int64, shape=[1, None], name="decode_seqs")
    net, net_rnn = model_seq2seq.model(encode_seqs2, decode_seqs2, xvocab_size, is_train=False, reuse=True)
    y = tf.nn.softmax(net.outputs)
    print("load_model() Reload model")
    # Load model trained
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    tl.layers.initialize_global_variables(sess)
    tl.files.load_and_assign_npz(sess=sess, name=os.path.join(settings.BASE_DIR, 'libs/seq2seq_core/', 'n.npz'),
                                 network=net)
    return sess, net_rnn, encode_seqs2, decode_seqs2, y


def pre_model(xvocab_size):
    # model_cache_key = 'model_cache'
    # sess = cache.get(model_cache_key)
    # net_rnn_cache_key = 'net_rnn_cache'
    # net_rnn = cache.get(net_rnn_cache_key)
    #
    # encode_seqs2_cache_key = 'encode_seqs2_cache'
    # encode_seqs2 = cache.get(encode_seqs2_cache_key)
    # decode_seqs2_cache_key = 'decode_seqs2_cache'
    # decode_seqs2 = cache.get(decode_seqs2_cache_key)
    #
    # y_cache_key = 'y_cache_key'
    # y = cache.get(y_cache_key)

    if True:
        sess, net_rnn, encode_seqs2, decode_seqs2, y = config_model()
        # cache.set(model_cache_key, sess, None)  # save in the cache
        # cache.set(net_rnn_cache_key, net_rnn, None)
        # cache.set(encode_seqs2_cache_key, encode_seqs2, None)
        # cache.set(decode_seqs2_cache_key, decode_seqs2, None)
        # cache.set(y_cache_key, y, None)

    return sess, net_rnn, encode_seqs2, decode_seqs2, y


sess_model, net_rnn, encode_seqs2, decode_seqs2, y = pre_model(xvocab_size)


def transformToVector(question, w2idx):
    question = stringPlaintextUtil.optimizeTextHTML(question)
    # vector = [w2idx[w] if (w in w2idx) else w2idx[model_config.UNK] for w in question.split(' ')]
    question = ViTokenizer.tokenize(question).strip()
    print('QUESTION SEGMENT ::::::',question)
    # TODO NTT question very long
    if len(question) > max_len_question:
        pass  # TODO NTT Shorten question string with semantics 's it https://123doc.org//document/4045605-nghien-cuu-va-phat-trien-phuong-phap-rut-gon-cau-tieng-viet-dua-tren-phuong-phap-hoc-khong-giam-sat.htm
    # data_processing.pad_seq(['Địa', 'chỉ', 'Trường_Đại', 'học', 'Sư', 'phạm', 'Kỹ_Thuật', 'nằm', 'ở', 'đâu', ''], w2idx, 20)
    vector = data_processing.pad_seq(question.split(' '), w2idx, max_len_question)
    # NTT TODO Optimize word not in dictionary but semantic mapping with word in dictionary, as example SPKT == Sư phạm Kỹ thuật
    logger.info('predict_seq2seq_core transformToVector ::: {question} -> to vector :: {vector}'
                .format(question=question, vector=vector))

    return vector


def formatAnswers(answer_text):
    # TODO NTT optimize string duplicate word predict, and format add token mark ,.!?...
    answer_text = answer_text.replace(model_config.PAD, model_config.SPA)
    answer_format = stringPlaintextUtil.optimizeText(answer_text)

    isResolve = True
    if len(answer_format) < 3:
        answer_format = model_config.NOT_RESOLVE_ANSWERS
        isResolve = False
    return answer_format, isResolve


# Main function Predict query string chatbots, input question string
def predict_chatbots(question):
    print("Chat Query >", question)
    seed_id = transformToVector(question, w2idx)

    # 1. encode, get state
    state = sess_model.run(net_rnn.final_state_encode,
                           {encode_seqs2: [seed_id]})
    # 2. decode, feed start_id, get first word
    #   ref https://github.com/zsdonghao/tensorlayer/blob/master/example/tutorial_ptb_lstm_state_is_tuple.py
    o, state = sess_model.run([y, net_rnn.final_state_decode],
                              {net_rnn.initial_state_decode: state,
                               decode_seqs2: [[start_id]]})
    # print('Output Decoder first :::', o)
    w_id = tl.nlp.sample_top(o[0], top_k=3) # DynamicRNNLayer output is [[]]
    w = idx2w[w_id]
    # print('Decoder ouput word first :::', w ,' idx ::',w_id)
    # 3. decode, feed state iteratively
    sentence = [w]

    # for _ in range(max_len_answers + 1):  # max sentence length +1 end_id; start find in first decoder/_
    while w_id != end_id:
        o, state = sess_model.run([y, net_rnn.final_state_decode],
                                  {net_rnn.initial_state_decode: state,
                                   decode_seqs2: [[w_id]]})
        w_id = tl.nlp.sample_top(o[0], top_k=2)
        w = idx2w[w_id]
        if w_id == end_id:
            break
        sentence = sentence + [w]
    result_predict, is_resolve = formatAnswers(' '.join(sentence))
    print("Chat Answer >", result_predict)
    return result_predict, is_resolve


# NTT TEST CORE SEQ2SEQ
if __name__ == '__main__':
    predict_chatbots('Đại học SPKT nằm ở đâu')
    predict_chatbots('Sư phạm kỹ thuật thành phố hồ chí minh ở đâu')
    predict_chatbots(
        'Sư phạm kỹ thuật thành phố hồ chí minh ở đâu ' +
        'Sư phạm kỹ thuật thành phố hồ chí minh ở đâu Sư phạm kỹ thuật thành phố hồ chí minh ở đâu\n' +
        'Sư phạm kỹ thuật thành phố hồ chí minh ở đâu')
    predict_chatbots('SPKT nằm ở đâu')
    predict_chatbots('Địa chỉ SPKT')
    predict_chatbots('Địa chỉ phạm kỹ thuật')
    predict_chatbots('Địa chỉ Đại học Sư phạm Kỹ Thuật')
    predict_chatbots('Địa chỉ Sư phạm kỹ thuật')
    predict_chatbots('Địa chỉ Sư phạm kỹ thuật thành phố Hồ Chí Minh')
    predict_chatbots('Sư phạm kỹ thuật')
    predict_chatbots('Số 01 Võ Văn Ngân Phường Linh Chiểu Thủ Đức thành phố Hồ Chí Minh')
