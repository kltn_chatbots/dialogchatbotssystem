import json

from libs.seq2seq_core.predict_seq2seq_core import predict_chatbots
from utils import stringPlaintextUtil

FILENAME = './data/WebDashboard_chatmessage.json'


def read_question_answers(filename):
    qlines = []
    alines = []

    with open(filename, encoding='utf-8') as f:
        data = json.load(f)
    for item in data:
        if len(item['questionProcessed'].split(' ')) <= 25:
            qlines.append(item['questionMessage'])
            alines.append(item['answerProcessed'])
    return qlines, alines


def count_word_same(answer, response):
    pass


def calculate_score(answer, response):
    len_answer = len(answer)
    len_response = len(response)


questions, answers = read_question_answers(filename=FILENAME)

response = []
count_not_resolve = 0
not_response = []
count_is_resolve = 0
for item in range(len(questions)):
    question = questions[item]
    answer = stringPlaintextUtil.optimizeText(answers[item])
    answer = answer.replace('_', ' ')

    result_answer, is_resolve = predict_chatbots(question)

    if is_resolve:
        response.append(
            {'item_data': [{'question': question}, {'answer': answer}, {'result': result_answer}]}
        )
        count_is_resolve += 1
    else:
        count_not_resolve += 1
        not_response.append(
            {'item_not_response': [{'question': question}, {'answer': answer}]})

with open('./data.json', 'w', encoding='utf-8') as outfile:
    json.dump({'result': response}, outfile, ensure_ascii=False)

with open('./data_not_response.json', 'w', encoding='utf-8') as outfile:
    json.dump({'result': not_response}, outfile, ensure_ascii=False)

print('count_not_resolve', count_not_resolve)
print('count_is_resolve', count_is_resolve)
