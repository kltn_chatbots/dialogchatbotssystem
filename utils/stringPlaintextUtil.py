import re

TAG_RE = re.compile(r'<[^>]+>')
STR_OP = re.compile(r"[-()\"#/@;:<>{}`+=~|.!?,]")


# remove tag html
def removeTags(text):
    return TAG_RE.sub('', text)

# remove mark ? ! ....
def removeMark(text):
    return STR_OP.sub('', text)

# remove space double
def optimizeText(text_string=''):
    return (' '.join(text_string.split())).strip()


# optimzie text full remove html tag and fix space string
def optimizeTextHTML(text_string=''):
    text_string = removeTags(text_string)
    text_string = removeMark(text_string)
    return (' '.join(text_string.split())).strip()
